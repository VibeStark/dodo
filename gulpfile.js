var gulp = require('gulp');

var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');

var data = require('gulp-data');
var pug = require('gulp-pug');
var path = require('path');
var fs = require('fs');

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var scriptsList = [
    './src/js/jquery.js',
    './src/js/jquery.sparkline.js',
    './src/js/script.js'
];

gulp.task('serve', ['watch'], function () {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });
});

gulp.task('watch', function () {
    gulp.watch(['./src/pug/**/*.pug', './src/pug/data.json'], ['pug']);
    gulp.watch(['./src/sass/**/*.scss'], ['sass']);
    gulp.watch('./src/js/**/*.js', ['scripts']);
});

gulp.task('sass', function () {
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});


gulp.task('pug', function buildHTML() {
    return gulp.src('./src/pug/*.pug')
        .pipe(data(function (file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        }))
        .pipe(
        pug({
            pretty: true
        })
    )
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function(){
    gulp.src(scriptsList)
        .pipe(concat('build.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
});

;(function(){
    $('.table__sparkline').sparkline('html', {
        type: 'bar',
        barWidth: 4,
        chartRangeMin: 0,
        chartRangeMax: 10,
        barSpacing: 0,
        colorMap: {
            '7': '#D2D6D9',
            '6': '#D2D6D9',
            '8': '#D2D6D9',
            '9': '#2F2F2F',
            '5': '#AC3B2D'
        }
    });
}());